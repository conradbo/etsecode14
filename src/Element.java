/*
 * Alfred Barbero Oliván
 * Conrad Barbero Oliván
 * Gerard Martí Sendra
 * 
 * 1r Concurs ETSECode URV 2014
 */
public class Element {
	
	String nomElement, nomOcult;
	boolean lliure;

	public Element(String nomElement, String nomOcult, boolean lliure){
		this.nomElement=nomElement;
		this.nomOcult=nomOcult;
		this.lliure=lliure;
	}
	
	public String getNomElement(){
		return nomElement;
	}
	
	public String getNomOcult(){
		return nomOcult;
	}
	
	public boolean getLliure(){
		return lliure;
	}
	
	public void setNomOcult(String nomOcult){
		this.nomOcult=nomOcult;
	}
	
	public void setNomElement(String nomElement){
		this.nomElement=nomElement;
	}

	public void setLliure(boolean lliure){
		this.lliure=lliure;
	}

}