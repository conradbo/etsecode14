/*
 * Alfred Barbero Oliván
 * Conrad Barbero Oliván
 * Gerard Martí Sendra
 * 
 * 1r Concurs ETSECode URV 2014
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;



public class main {
static int MAX_PARELLES = 50;
static int MAX_CARTES = 100;
int MIN_PARELLES = 3;
int MIN_CARTES = 6;
static Element [][] tauler = new Element [10][10];
static String[] elements = {"H ","He","Li","Be","B ","C ","N ","O ","F ","Ne","Na","Mg","Al","Si","P ","S ","Cl","Ar","K ","Ca","Sc","Ti","V ","Cr","Mn","Fe","Co","Ni","Cu","Zn","Ga","Ge","As","Se","Br","Kr","Rb","Sr","Y ","Zr","Nb","Mo","Tc","Ru","Rh","Pd","Ag","Cd","In","Sn"};
	
	public static void main(String[] args) throws IOException {		
		omplirTauler(10,10);
		omplirTaulerElement();
		System.out.println("Hi ha "+MAX_PARELLES+" parelles en joc.\n");
		mostrarTauler();
		System.out.println("\nPrem c per continuar");
		InputStreamReader entrada = new InputStreamReader(System.in);
		BufferedReader teclat = new BufferedReader(entrada);
		String pulsacio = teclat.readLine();
		if ((pulsacio.equals("c")) || (pulsacio.equals("C"))){
			for (int i=0;i<30;i++){
				System.out.println("");//Deixem espais per "amagar" la matriu original
			}
			mostrarTaulerOcult();
		}
		
	}//fi main
	
	public static void omplirTauler(int nFil, int nCol){
		//Omple el tauler amb els noms ocults
		char nom;
		String nomOcult;
		int auxSuma;
		for(int i=0;i<nFil;i++){
			auxSuma = i+97;	//97 es el primer valor ascii de les lletres minuscules, a
			nom = (char)auxSuma;
			for(int j=0;j<nCol;j++){
				nomOcult = ""+nom+""+j;
				//System.out.println(i+"  "+j);
				tauler[i][j]=new Element("",nomOcult, true);
			}//fi for j
		}//fi for i
	}//fi omplirTauler
	
	public static void omplirTaulerElement(){
		//Omple el tauler amb els elements de la taula periodica
		int pdi=0;
		boolean afegit = false;
		int contSaturat = 0;
		for(int i=0;i<2;i++){
			for(pdi=0;pdi<50;pdi++){
				afegit = false;
					while (!afegit){
						int filRan = (int)(Math.random()*10);
						int colRan = (int)(Math.random()*10);
						//System.out.println(filRan+"  "+colRan);
						if (tauler[filRan][colRan].getLliure()==true){
							tauler[filRan][colRan].setNomElement(elements[pdi]);
							tauler[filRan][colRan].setLliure(false);	
							afegit=true;
						}
						contSaturat++;
						if (contSaturat>10){
							int fil, col, num;
							num = calcularLliure();
							fil=num/100;
							col=num-fil*100;
							tauler[fil][col].setNomElement(elements[pdi]);
							tauler[fil][col].setLliure(false);
							
							afegit=true;
						}
					}//fi while			
				}//fi for pdi(elements)
		}//fi for i(2 cartes per element)
	}//fi omplirTaulerElement
	
	public static int calcularLliure(){
	//calcular primera posicio lliure del tauler, en cas que el random colisioni mes de 10 vegades
	int posicio = 0;
		for(int i=0;i<10;i++){
			for(int j=0;j<10;j++){
				if (tauler[i][j].getLliure()==true){
					posicio = i*100+j;
					i=11;
					j=11;
				}	
			}//fi for j
		}//fi for i
		return (posicio);
	}//fi calcularLliure
	
	public static void mostrarTaulerOcult(){
		for(int i=0;i<10;i++){
			for(int j=0;j<10;j++){
				System.out.print(tauler[i][j].getNomOcult()+"     ");
			}
			System.out.println("");
		}		
	}
	
	public static void mostrarTauler(){
		for(int i=0;i<10;i++){
			for(int j=0;j<10;j++){
				System.out.print(tauler[i][j].getNomElement()+"     ");
			}
			System.out.println("");
		}
	}
}//fi class
